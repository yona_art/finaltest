package com.sofftek.academy.Unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.sofftek.academy.Beans.UserMonthBean;
import com.sofftek.academy.Controllers.UserController;
import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Services.PersonService;
import com.sofftek.academy.Services.RegisterService;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

/**
 * PruebasUnitariasTest
 */
@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class PruebasUnitariasTest {

    @Autowired
    private UserController userController;

    @Autowired
    private PersonService personService; 

    @Autowired
    private RegisterService registerService;

    //Se agrego la prueba unitaria pero se tiene problemas con el procesamiento de la base datos 
    // al parecer era necesario agregar una libreria de jpa testing pero no pude hacerlo funcionar.
    @Test
    public void readExcelTest() throws Exception, IOException {
        // Setup
        MockMultipartFile file = new MockMultipartFile("Registros Momentum.xlsx",
                new FileInputStream(new File("src/test/resources/Registros Momentum.xlsx")));
        // Execute
        ResponseEntity<String> response = userController.importData(file);
        // Verify
        assertNotNull("Read the file has error ", response);
        assertEquals(new ResponseEntity<>(HttpStatus.OK), response);
    }


    @Test
    public void readExcelLocalTest() throws Exception, IOException {
        // Setup
        MockMultipartFile file = new MockMultipartFile("Registros Momentum.xlsx",
                new FileInputStream(new File("src/test/resources/Registros Momentum.xlsx")));
        int expectedSheets = 1;
        // Execute
        Workbook excel = WorkbookFactory.create(file.getInputStream());
        // Verify
        assertNotNull("Read the file has error ", excel);
        assertEquals("The sheets are not same number",expectedSheets, excel.getNumberOfSheets());
    }


    @Test
    public void finuserByISTest() throws ParseException {
        // Setup
        String is = "CASV1";
        boolean Expectedexists = true;
        // Execute
        boolean Actualexists = personService.existsPersonByName(is);
        // Verify
        assertEquals("The user not exists", Expectedexists, Actualexists);
    }

    @Test
    public void getUsersByDatesTest() throws Exception {
        // Setup
        List<Register> expectedRegisters = new ArrayList<>();
        UserMonthBean user = new UserMonthBean("CASV1", "2019-01-07 08:30:01", "2019-02-25 17:36:21");
        List<Register> actualRegisters = registerService.getRegistersByIS(user.getIs());
        // Execute
        expectedRegisters = registerService.getRegisterByDatesBetweenAndIs( 
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(user.getStartDate()),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(user.getEndDate()),
                user.getIs());  
        // Verify
        assertNotSame("The List are same", expectedRegisters, actualRegisters);
    }

}