package com.sofftek.academy.Int;

import static org.mockito.Mockito.when;

import java.sql.Date;
import java.text.ParseException;
import java.time.Year;
import java.time.YearMonth;
import java.util.List;

import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Repositories.PersonRepository;
import com.sofftek.academy.Repositories.RegisterRepository;
import com.sofftek.academy.Services.RegisterService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * IntegrationTests
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootConfiguration
public class IntegrationTests {

    @Mock
    RegisterRepository registerRepository;

    @Mock
    PersonRepository PersonRepository;

    @InjectMocks
    RegisterService registerService;

    @Test
    public void getRegisterByMonthTest() throws ParseException {
        // Setup
        String is = "CASV1";
        int month = 1;
        YearMonth yearMonth = YearMonth.of(Year.now().getValue()-1, month);
        List<Register> expectedRegister = registerService.getRegistersByIS(is);
        //Execute
        when(registerService.getRegisterByDatesBetweenAndIs(
            Date.valueOf(yearMonth.atDay(1)),
             Date.valueOf(yearMonth.atEndOfMonth()), is)).thenReturn(expectedRegister);

        
    }

}