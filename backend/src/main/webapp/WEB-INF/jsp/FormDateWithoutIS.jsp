<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form" %>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Busqueda Por Fechas</title>
	</head>
	<body>
		<form:form method="POST" action ="/api/v1/ListByDatesWithoutIS" modelAttribute="values">
			<table>
				<tr>
					<td> <form:label path = "startDate">Inicio: </form:label></td>
					<td> <form:input type="date" path = "startDate"></form:input></td>
				</tr>
				<tr>
					<td> <form:label path = "endDate">Fin: </form:label></td>
					<td> <form:input type="date" path = "endDate"></form:input></td>
				</tr>
				<tr>
					<td colspan= "2">
						<input type = "submit" value = "Submit"/>
					</td>
				</tr>
			</table>
		</form:form>

		<a href="/api/v1/">Index</a>
	</body>
</html>
