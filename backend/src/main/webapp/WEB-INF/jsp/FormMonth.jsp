<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form" %>
    
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Selecion de año y mes</title>
	</head>
	<body>
		<form:form method="POST" action ="/api/v1/redirect" modelAttribute="values">
			<table>
				<tr>
					<td> <form:label path = "startDate">Month: </form:label></td>
					<td> <form:input type="number" min="1" max="12" path = "startDate"></form:input></td>
				</tr>
					<tr>
					<td> <form:label path = "endDate">Year: </form:label></td>
					<td> <form:input type="number" min="2000" max="5000"  path = "endDate"></form:input></td>
				</tr>
				<%-- <tr>
					<td> <form:label path = "is">IS: </form:label></td>
					<td> <form:input path = "is"></form:input></td>
				</tr> --%>
				<tr>
					<td colspan= "2">
						<input type = "submit" value = "Submit"/>
					</td>
				</tr>
			</table>
		</form:form>

		<a href="/api/v1/">Index</a>
	</body>
</html>
