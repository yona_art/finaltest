<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="ISO-8859-1">
    <meta charset="UTF-8">
    <title>List de registros</title>
</head>

<body>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Departament</th>
                <th scope="col">Name</th>
                <th scope="col">ID</th>
                <th scope="col">Date/Time</th>
                <th scope="col">Verifycode</th>
                <th scope="col">Clock-in/out</th>
                <th scope="col">Device ID</th>
                <th scope="col">Device Name</th>
                <th scope="col">UserExtFmt</th>
            </tr>
        </thead>
        <tbody>

            <c:forEach var="item" items="${items}">
                <tr>
                    <th scope="row">${item.getPerson().getNumber()}</th>
                    <td>${item.getDepartament().getName()}</td>
                    <td>${item.getPerson().getis()}</td>
                    <td>${item.getPerson().getId()}</td>
                    <td>${item.getDate()}</td>
                    <td>${item.getClock().getName()}</td>
                    <td>${item.getDevice().getId()}</td>
                    <td>${item.getDevice().getVerifyCode()}</td>
                    <td>${item.getDevice().getBuild().getName()}</td>
                    <td>${item.getUserExtFmt()}</td>
                </tr>
            </c:forEach>


        </tbody>
    </table>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>

</html>