package com.sofftek.academy.Beans;

/**
 * UserMonth
 */
public class UserMonthBean {

    private String IS;
    private String startDate;
    private String endDate;

    public UserMonthBean() {

    }

    public UserMonthBean(String IS, String startDate, String endDate) {
        this.IS = IS;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    public String getIs() {
        return IS;
    }

    public void setIs(String IS) {
        this.IS = IS;
    }

    
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}