package com.sofftek.academy.Beans;

import org.springframework.web.multipart.MultipartFile;

/**
 * FileBean
 */
public class FileBean {

    private MultipartFile file;

    public FileBean() {

    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}