package com.sofftek.academy.Controllers;

import java.time.YearMonth;
import java.util.Arrays;
import java.util.List;
import java.sql.Date;
import java.text.ParseException;

import com.sofftek.academy.Beans.FileBean;
import com.sofftek.academy.Beans.UserMonthBean;
import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Services.RegisterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * ViewController
 */
@Controller
public class ViewController {


    @Autowired
    private RegisterService registerService;

    @Autowired
    private UserController userController;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(Model model) {
        ModelAndView view = new ModelAndView("index");
        return view;
    }

    @GetMapping("/hours")
    public ModelAndView formByHours() {
        return new ModelAndView("FormMonthAndIS", "values", new UserMonthBean());
    }

    @PostMapping(value = "/hoursRedirect")
    public String getlistPerson(@ModelAttribute("values") UserMonthBean data) {
        return "redirect:/ListByHours/" + data.getIs() + "/" + data.getStartDate() + "/" + data.getEndDate();
    }

    @GetMapping(value = "/ListByHours/{IS}/{M}/{Y}")
    public ModelAndView getRegisterByMonth(@PathVariable(value = "IS", required = false) String IS,
            @PathVariable(value = "M", required = false) Integer month,
            @PathVariable(value = "Y", required = false) Integer year) {
        ModelAndView view = new ModelAndView("ListByDatesAndHours");
        YearMonth yearMonth = YearMonth.of(year, month);
        List<Register> items = registerService.getRegisterByDatesBetweenAndIs(Date.valueOf(yearMonth.atDay(1)),
                Date.valueOf(yearMonth.atEndOfMonth()), IS);
        ResponseEntity<Long> hours = userController.getHoursForMonth(IS, month);
        view.addObject("items", items);
        view.addObject("hours", hours.getBody());
        return view;
    }

    // Termina el primer endpoint

    @RequestMapping(value = "/formDates", method = RequestMethod.GET)
    public ModelAndView formDates() {
        return new ModelAndView("FormDates", "values", new UserMonthBean());
    }

    @PostMapping(value = "/ListByDates")
    public ModelAndView ListByDates(@ModelAttribute("values") UserMonthBean data) throws ParseException {
        ModelAndView view = new ModelAndView("ListByDates");
        List<String> dateStart = Arrays.asList(data.getStartDate().split("-"));
        List<String> dateEnd = Arrays.asList(data.getEndDate().split("-"));

        YearMonth startDate = YearMonth.of(Integer.parseInt(dateStart.get(0)), Integer.parseInt(dateStart.get(1)));
        YearMonth endDate = YearMonth.of(Integer.parseInt(dateEnd.get(0)), Integer.parseInt(dateEnd.get(1)));

        List<Register> items = registerService.getRegisterByDatesBetweenAndIs(
                Date.valueOf(startDate.atDay(Integer.parseInt(dateStart.get(2)))),
                Date.valueOf(endDate.atDay(Integer.parseInt(dateEnd.get(2)))), data.getIs());
        view.addObject("items", items);
        return view;
    }

    // termina el segundo endpoint

    @RequestMapping(value = "/formMonths", method = RequestMethod.GET)
    public ModelAndView formMothscharge() {
        return new ModelAndView("FormMonth", "values", new UserMonthBean());
    }

    @PostMapping(value = "/redirect")
    public String RedirectList(@ModelAttribute("values") UserMonthBean data) throws ParseException {
        return "redirect:/listBase/" + data.getStartDate() + "/" + data.getEndDate();
    }

    @GetMapping(value = "/listBase/{M}/{Y}")
    public ModelAndView listBase(@PathVariable(value = "M", required = false) Integer month,
            @PathVariable(value = "Y", required = false) Integer year) {
        ModelAndView view = new ModelAndView("ListBase");
        YearMonth yearMonth = YearMonth.of(year, month);
        List<Register> items = registerService.getRegisterByDate(Date.valueOf(yearMonth.atDay(1)),
                Date.valueOf(yearMonth.atEndOfMonth()));
        view.addObject("items", items);
        return view;
    }

    // Termine el tercer endpoint

    @RequestMapping(value = "/formDateWithoutIS", method = RequestMethod.GET)
    public ModelAndView formDateWithoutIS() {
        return new ModelAndView("FormDateWithoutIS", "values", new UserMonthBean());
    }

    @PostMapping(value = "/ListByDatesWithoutIS")
    public ModelAndView ListByDatesWitoutIS(@ModelAttribute("values") UserMonthBean data) throws ParseException {
        ModelAndView view = new ModelAndView("ListBase");

        List<String> dateStart = Arrays.asList(data.getStartDate().split("-"));
        List<String> dateEnd = Arrays.asList(data.getEndDate().split("-"));

        YearMonth startDate = YearMonth.of(Integer.parseInt(dateStart.get(0)), Integer.parseInt(dateStart.get(1)));
        YearMonth endDate = YearMonth.of(Integer.parseInt(dateEnd.get(0)), Integer.parseInt(dateEnd.get(1)));

        List<Register> items = registerService.getRegisterByDate(
                Date.valueOf(startDate.atDay(Integer.parseInt(dateStart.get(2)))),
                Date.valueOf(endDate.atDay(Integer.parseInt(dateEnd.get(2)))));
        view.addObject("items", items);
        return view;
    }

    // Termina el cuarto endpoint

    @GetMapping(value = "/import")
    public ModelAndView getMethodName() {
        return new ModelAndView("Import", "values", new FileBean());
    }

    @RequestMapping(value = "/importFile", method = RequestMethod.POST)
    public ModelAndView importData(@ModelAttribute("values") FileBean data) throws Exception {
        userController.importData(data.getFile());
        return new ModelAndView("index");
    }
    // Termina el endpoint de cargar achivos

}