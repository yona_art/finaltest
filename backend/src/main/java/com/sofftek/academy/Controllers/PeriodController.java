package com.sofftek.academy.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Year;
import java.time.YearMonth;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.sofftek.academy.Beans.UserMonthBean;
import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Services.RegisterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * PeriodController
 */
@RestController
@RequestMapping("period")
public class PeriodController {

    @Autowired
    private RegisterService registerService;

    @CrossOrigin
    @GetMapping(value = "/{M}")
    public ResponseEntity<List<Register>> getRegisterByMonth(@PathVariable(value = "M", required = false) Integer month) {
        YearMonth yearMonth = YearMonth.of(Year.now().getValue(), month);
        List<Register> items = registerService.getRegisterByDate(Date.valueOf(yearMonth.atDay(1)),
                Date.valueOf(yearMonth.atEndOfMonth()));
        if (items.size() > 0) {
            return new ResponseEntity<List<Register>>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @PostMapping(value = "/")
    public ResponseEntity<List<Register>> getUsersByDates(@RequestBody UserMonthBean data) throws ParseException {
        List<Register> items = registerService.getRegisterByDate(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data.getStartDate()),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data.getEndDate()));

        if (items.size() > 0) {
            return new ResponseEntity<List<Register>>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}