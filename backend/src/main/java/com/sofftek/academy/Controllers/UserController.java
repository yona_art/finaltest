package com.sofftek.academy.Controllers;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import com.sofftek.academy.Beans.UserMonthBean;
import com.sofftek.academy.Models.Build;
import com.sofftek.academy.Models.Clock;
import com.sofftek.academy.Models.Departament;
import com.sofftek.academy.Models.Device;
import com.sofftek.academy.Models.Person;
import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Services.BuildService;
import com.sofftek.academy.Services.ClockService;
import com.sofftek.academy.Services.DepartamentService;
import com.sofftek.academy.Services.DeviceService;
import com.sofftek.academy.Services.PersonService;
import com.sofftek.academy.Services.RegisterService;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * UsersController
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private RegisterService registerService;

    @Autowired
    private DepartamentService departamentService;

    @Autowired
    private PersonService personService;

    @Autowired
    private ClockService clockservice;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private BuildService buildService;

    @CrossOrigin
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity<String> importData(@RequestParam(value = "file", required = true) MultipartFile file)
            throws Exception {
        DataFormatter dataFormatter = new DataFormatter();
        Workbook excel = WorkbookFactory.create(file.getInputStream());

        excel.forEach(sheet -> {
            sheet.forEach(row -> {
                if (row.getRowNum() != 0) {
                    List<String> item = new ArrayList<String>();
                    row.forEach((cell) -> {
                        item.add(dataFormatter.formatCellValue(cell));
                    });
                    if (item.size() >= 8) {
                        Register newRegister = new Register();

                        newRegister.setUserExtFmt(item.get(9));
                        try {
                            // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            newRegister.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.get(4)));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (departamentService.existsDepartamentByName(item.get(1))) {
                            newRegister.setDepartament(departamentService.getDepartamentByName(item.get(1)));
                        } else {
                            newRegister
                                    .setDepartament(departamentService.createDepartament(new Departament(item.get(1))));
                        }

                        if (personService.existsPersonByNumber(Integer.parseInt(item.get(0)))) {
                            newRegister.setPerson(personService.getPersonBynumber(Integer.parseInt(item.get(0))));
                        } else {
                            newRegister.setPerson(personService.createPerson(new Person(Integer.parseInt(item.get(3)),
                                    item.get(2), Integer.parseInt(item.get(0)))));
                        }

                        if (deviceService.existsDeviceById(Integer.parseInt(item.get(7)))) {
                            newRegister.setDevice(deviceService.getDeviceById(Integer.parseInt(item.get(7))));
                        } else {
                            Device newDevice = new Device();
                            boolean value = buildService.existsBuildByName(item.get(8));
                            if (value) {
                                Build builds = buildService.getBuildByName(item.get(8));
                                newDevice.setBuild(builds);
                                newRegister.setDevice(newDevice);
                            } else {
                                newDevice.setBuild(buildService.createBuild(new Build(item.get(8))));
                                newDevice.setVerifyCode(item.get(5));
                                newDevice.setId(Integer.parseInt(item.get(7)));
                                newRegister.setDevice(deviceService.createDevice(newDevice));
                            }
                        }
                        if (clockservice.existsClockByName(item.get(6))) {
                            newRegister.setClock(clockservice.getClocktByName(item.get(6)));
                        } else {
                            newRegister.setClock(clockservice.createClock(new Clock(item.get(6))));
                        }
                        registerService.createRegister(newRegister);
                    }
                }

            }); 
        });
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/{IS}/{M}")
    public ResponseEntity<Long> getHoursForMonth(@PathVariable(value = "IS", required = false) String IS,
            @PathVariable(value = "M", required = false) Integer month) {
        YearMonth yearMonth = YearMonth.of(Year.now().getValue()-1, month);
        List<Register> items = registerService.getRegisterByDatesBetweenAndIs(Date.valueOf(yearMonth.atDay(1)),
                Date.valueOf(yearMonth.atEndOfMonth()), IS);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        Long finalHours = 0L;

        for (int i = 0; i < items.size(); i++) {
            Long hoursadding = 0L;
            int indexAdding = 0;

            if ((i + 1) < items.size()) {
                if (fmt.format(items.get(i).getDate()).equals(fmt.format(items.get(i + 1).getDate()))) {
                    if ((items.get(i).getClock().getId() == 2 || items.get(i).getClock().getId() == 1)
                            && (items.get(i + 1).getClock().getId() == 2 || items.get(i + 1).getClock().getId() == 1)) {
                        Long diff = items.get(i).getDate().getTime() - items.get(i + 1).getDate().getTime();
                        hoursadding = diff / (60 * 60 * 1000) % 24;
                        hoursadding += (diff / (60 * 1000) % 60) / 60;
                        indexAdding = 1;
                    }
                }
            } else if ((i + 2) < items.size()) {
                if (fmt.format(items.get(i).getDate()).equals(fmt.format(items.get(i + 2).getDate()))) {
                    if ((items.get(i).getClock().getId() == 2 || items.get(i).getClock().getId() == 1)
                            && (items.get(i + 2).getClock().getId() == 2 || items.get(i + 2).getClock().getId() == 1)) {
                        Long diff = items.get(i).getDate().getTime() - items.get(i + 2).getDate().getTime();
                        hoursadding = diff / (60 * 60 * 1000) % 24;
                        hoursadding += (diff / (60 * 1000) % 60) / 60;
                        indexAdding = 2;
                    }
                } 
            }

            if (i >= items.size()) {
                break;
            }
            i += indexAdding;
            finalHours += hoursadding;
        }
        finalHours = (finalHours * -1);
        if (finalHours > 0) {
            return new ResponseEntity<Long>(finalHours, HttpStatus.OK);
        } else {
            return new ResponseEntity<Long>(0L, HttpStatus.NOT_FOUND);
        }

    }
    
    @CrossOrigin
    @PostMapping(value = "/")
    public ResponseEntity<List<Register>> getUsersByDatesWithIS(@RequestBody UserMonthBean data) throws ParseException {
        List<Register> items = registerService.getRegisterByDatesBetweenAndIs(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data.getStartDate()),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data.getEndDate()), data.getIs());

        if (items.size() > 0) {
            return new ResponseEntity<List<Register>>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}