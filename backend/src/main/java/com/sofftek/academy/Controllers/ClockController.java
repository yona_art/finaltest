package com.sofftek.academy.Controllers;

import java.util.List;

import com.sofftek.academy.Models.Clock;
import com.sofftek.academy.Services.ClockService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClockController
 */
@RestController
@RequestMapping("clocks")
public class ClockController {

    @Autowired
    private ClockService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<List<Clock>> getAllClocks() {
        List<Clock> items = service.getAll();
        if (items.size() > 0) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<Clock> getClockById(@PathVariable("id") int id) {
        Clock items = service.getClockById(id);
        if (!items.equals(null)) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/JSON")
    public ResponseEntity<Clock> createClock(@RequestBody Clock Clock) {
        Clock item = service.createClock(Clock);
        if (!item.equals(null)) {
            return new ResponseEntity<>(item, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = "application/JSON")
    public ResponseEntity<String> deleteClock(@PathVariable("id") int id) {
        this.service.deleteClock(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/JSON")
    public ResponseEntity<String> updateClock(@PathVariable("id") int id, @RequestBody Clock Clock) {
        this.service.updateClock(id, Clock);
        return new ResponseEntity<>("Updated", HttpStatus.OK);
    }
    
}