package com.sofftek.academy.Controllers;

import java.util.List;

import com.sofftek.academy.Models.Departament;
import com.sofftek.academy.Services.DepartamentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * DepartamentController
 */
@RestController
@RequestMapping("departaments")
public class DepartmentController {

    @Autowired
    private DepartamentService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<List<Departament>> getAllDepartaments() {
        List<Departament> items = service.getAll();
        if (items.size() > 0) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<Departament> getDepartamentById(@PathVariable("id") int id) {
        Departament items = service.getDepartamentById(id);
        if (!items.equals(null)) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/JSON")
    public ResponseEntity<Departament> createDepartament(@RequestBody Departament Departament) {
        Departament item = service.createDepartament(Departament);
        if (!item.equals(null)) {
            return new ResponseEntity<>(item, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = "application/JSON")
    public ResponseEntity<String> deleteDepartament(@PathVariable("id") int id) {
        this.service.deleteDepartament(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/JSON")
    public ResponseEntity<String> updateDepartament(@PathVariable("id") int id, @RequestBody Departament Departament) {
        this.service.updateDepartament(id, Departament);
        return new ResponseEntity<>("Updated", HttpStatus.OK);
    }
    
}