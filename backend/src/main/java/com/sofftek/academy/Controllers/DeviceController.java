package com.sofftek.academy.Controllers;

import java.util.List;

import com.sofftek.academy.Models.Device;
import com.sofftek.academy.Services.DeviceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * DeviceController
 */
@RestController
@RequestMapping("devices")
public class DeviceController {

    @Autowired
    private DeviceService service;

    
    @RequestMapping(value = "/", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<List<Device>> getAllDevices() {
        List<Device> items = service.getAll();
        if (items.size() > 0) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<Device> getDeviceById(@PathVariable("id") int id) {
        Device items = service.getDeviceById(id);
        if (!items.equals(null)) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/JSON")
    public ResponseEntity<Device> createDevice(@RequestBody Device Device) {
        Device item = service.createDevice(Device);
        if (!item.equals(null)) {
            return new ResponseEntity<>(item, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = "application/JSON")
    public ResponseEntity<String> deleteDevice(@PathVariable("id") int id) {
        this.service.deleteDevice(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/JSON")
    public ResponseEntity<String> updateDevice(@PathVariable("id") int id, @RequestBody Device Device) {
        this.service.updateDevice(id, Device);
        return new ResponseEntity<>("Updated", HttpStatus.OK);
    }
}