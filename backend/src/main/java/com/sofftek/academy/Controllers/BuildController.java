package com.sofftek.academy.Controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.sofftek.academy.Models.Build;
import com.sofftek.academy.Services.BuildService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
/**
 * BuildController
 */
@RestController
@RequestMapping("builds")
public class BuildController {

    @Autowired
    private BuildService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<List<Build>> getAllBuilds() {
        List<Build> items = service.getAll();
        if (items.size() > 0) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<Build> getBuildById(@PathVariable("id") int id) {
        Build items = service.getBuildById(id);
        if (!items.equals(null)) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/JSON")
    public ResponseEntity<Build> createBuild(@RequestBody Build build) {
        Build item = service.createBuild(build);
        if (!item.equals(null)) {
            return new ResponseEntity<>(item, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = "application/JSON")
    public ResponseEntity<String> deleteBuild(@PathVariable("id") int id) {
        this.service.deleteBuild(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/JSON")
    public ResponseEntity<String> updateBuild(@PathVariable("id") int id, @RequestBody Build build) {
        this.service.updateBuild(id, build);
        return new ResponseEntity<>("Updated", HttpStatus.OK);
    }

}