package com.sofftek.academy.Controllers;

import java.util.List;

import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Services.RegisterService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * RegisterController
*/
@RestController
@RequestMapping("registers")
public class RegisterController {

    @Autowired
    private RegisterService service;

    @RequestMapping(value = "/", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<List<Register>> getAllRegisters() {
        List<Register> items = service.getAll();
        if (items.size() > 0) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, consumes = "application/JSON")
    public ResponseEntity<Register> getRegisterById(@PathVariable("id") int id) {
        Register items = service.getRegisterById(id);
        if (!items.equals(null)) {
            return new ResponseEntity<>(items, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/JSON")
    public ResponseEntity<Register> createRegister(@RequestBody Register Register) {
        Register item = service.createRegister(Register);
        if (!item.equals(null)) {
            return new ResponseEntity<>(item, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = "application/JSON")
    public ResponseEntity<String> deleteRegister(@PathVariable("id") int id) {
        this.service.deleteRegister(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, consumes = "application/JSON")
    public ResponseEntity<String> updateRegister(@PathVariable("id") int id, @RequestBody Register Register) {
        this.service.updateRegister(id, Register);
        return new ResponseEntity<>("Updated", HttpStatus.OK);
    }
}