package com.sofftek.academy.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Person
 */
@Entity
@Table(name = "Persons")
public class Person {

    @Id
    @JsonProperty
    private int id;
    @JsonProperty
    @Column(name = "name")
    private String is;
    @Column(name = "number")
    private int number;

    public Person() {
    }

    public Person(String is) {
        this.is = is;
    }

    public Person(int id, String is, int number) {
        this.id = id;
        this.is = is;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getis() {
        return is;
    }

    public void setis(String is) {
        this.is = is;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}