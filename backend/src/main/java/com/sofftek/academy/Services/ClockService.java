package com.sofftek.academy.Services;

import java.util.List;

import com.sofftek.academy.Models.Clock;
import com.sofftek.academy.Repositories.ClockRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClockService
 */
@Service
public class ClockService {

    @Autowired
    private ClockRepository repository;

    public List<Clock> getAll() {
        return repository.findAll();
    }

    public Clock getClockById(int id) {
        return repository.findById(id).get();
    }

    public Clock getClocktByName(String name) {
        return repository.findByName(name);
    }

    public boolean existsClockByName(String name){
        return repository.existsByName(name);
    }

    public Clock createClock(Clock Clock) {
        return repository.save(Clock);
    }

    public void updateClock(int id, Clock Clock) {
        Clock ClockActual = repository.findById(id).get();
        ClockActual.setId(Clock.getId());
        ClockActual.setName(Clock.getName());
        repository.save(ClockActual);
    }

    public void deleteClock(int id) {
        repository.deleteById(id);
    }
}