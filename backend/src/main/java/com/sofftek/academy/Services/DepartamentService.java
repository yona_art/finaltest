package com.sofftek.academy.Services;

import java.util.List;

import com.sofftek.academy.Models.Departament;
import com.sofftek.academy.Repositories.DepartamentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DepartamentService
 */
@Service
public class DepartamentService {

    @Autowired
    private DepartamentRepository repository;

    public List<Departament> getAll() {
        return repository.findAll();
    }

    public Departament getDepartamentById(int id) {
        return repository.findById(id).get();
    }

    public Departament getDepartamentByName(String name) {
        return repository.findByName(name);
    }

    public boolean existsDepartamentByName(String name){
        return repository.existsByName(name);
    }

    public Departament createDepartament(Departament Departament) {
        return repository.save(Departament);
    }

    public void updateDepartament(int id, Departament Departament) {
        Departament DepartamentActual = repository.findById(id).get();
        DepartamentActual.setId(Departament.getId());
        DepartamentActual.setName(Departament.getName());
        repository.save(DepartamentActual);
    }

    public void deleteDepartament(int id) {
        repository.deleteById(id);
    }
}