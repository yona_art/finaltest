package com.sofftek.academy.Services;

import java.util.Date;
import java.util.List;

import com.sofftek.academy.Models.Person;
import com.sofftek.academy.Models.Register;
import com.sofftek.academy.Repositories.PersonRepository;
import com.sofftek.academy.Repositories.RegisterRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * RegisterService
 */
@Service
public class RegisterService {

    @Autowired
    private RegisterRepository repository;

    @Autowired
    private PersonRepository personRepository;

    public List<Register> getAll() {
        return repository.findAll();
    }

    public Register getRegisterById(int id) {
        return repository.findById(id).get();
    }

    public boolean existsRegisterById(int id) {
        return repository.existsById(id);
    }

    public Register createRegister(Register Register) {
        return repository.save(Register);
    }

    public List<Register> getRegistersByIS(String IS) {
        return repository.findByperson(personRepository.findByis(IS));
    }

    public List<Register> getRegisterByDatesBetweenAndIs(Date start, Date end, String IS) {
        Person person = personRepository.findByis(IS);
        return repository.findBydateBetweenAndPerson(start, end, person);
    }

    public List<Register> getRegisterByDate(Date start, Date end) {
        return repository.findBydateBetween(start, end);
    }

    public void updateRegister(int id, Register Register) {
        Register RegisterActual = repository.findById(id).get();
        RegisterActual.setId(Register.getId());
        RegisterActual.setDate(Register.getDate());
        RegisterActual.setDepartament(Register.getDepartament());
        RegisterActual.setPerson(Register.getPerson());
        RegisterActual.setDevice(Register.getDevice());
        RegisterActual.setClock(Register.getClock());
        RegisterActual.setUserExtFmt(Register.getUserExtFmt());
        repository.save(RegisterActual);
    }

    public void deleteRegister(int id) {
        repository.deleteById(id);
    }
}