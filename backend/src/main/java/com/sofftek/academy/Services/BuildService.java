
package com.sofftek.academy.Services;

import java.util.List;

import com.sofftek.academy.Models.Build;
import com.sofftek.academy.Repositories.BuildRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * BuildService
 */
@Service
public class BuildService {

    @Autowired
    private BuildRepository repository;

    public List<Build> getAll() {
        return repository.findAll();
    }

    public Build getBuildById(int id) {
        return repository.findById(id).get();
    }

    public Build getBuildByName(String name){
        return repository.findByname(name);
    }

    public boolean existsBuildByName(String name){
        return repository.existsByName(name);
    }

    public Build createBuild(Build build) {
        return repository.save(build);
    }

    public void updateBuild(int id, Build build) {
        Build buildActual = repository.findById(id).get();
        buildActual.setId(build.getId());
        buildActual.setName(build.getName());
        repository.save(buildActual);
    }

    public void deleteBuild(int id) {
        repository.deleteById(id);
    }
}