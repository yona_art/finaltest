package com.sofftek.academy.Services;

import java.util.List;

import com.sofftek.academy.Models.Person;
import com.sofftek.academy.Repositories.PersonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PersonService
 */
@Service
public class PersonService {

    @Autowired
    private PersonRepository repository;
    
    public List<Person> getAll() {
        return repository.findAll();
    }

    public Person getPersonById(int id) {
        return repository.findById(id).get();
    }
    public Person getPersonByName(String is){
        return repository.findByis(is);
    }

    public Person getPersonBynumber(int number){
        return repository.findBynumber(number);
    }

    public boolean existsPersonById(int id){
        return repository.existsById(id);
    }

    public boolean existsPersonByName(String name){
        return repository.existsByis(name);
    }
    public boolean existsPersonByNumber(int number){
        return repository.existsBynumber(number);
    }

    public Person createPerson(Person Person) {
        return repository.save(Person);
    }

    public void updatePerson(int id, Person Person) {
        Person PersonActual = repository.findById(id).get();
        PersonActual.setId(Person.getId());
        PersonActual.setis(Person.getis());
        PersonActual.setNumber(Person.getNumber());
        repository.save(PersonActual);
    }

    public void deletePerson(int id) {
        repository.deleteById(id);
    }
}