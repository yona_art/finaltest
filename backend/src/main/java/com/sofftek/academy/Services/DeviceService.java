package com.sofftek.academy.Services;

import java.util.List;

import com.sofftek.academy.Models.Device;
import com.sofftek.academy.Repositories.DeviceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DeviceService
 */
@Service
public class DeviceService {

    @Autowired
    private DeviceRepository repository;

    public List<Device> getAll() {
        return repository.findAll();
    }

    public Device getDeviceById(int id) {
        return repository.findById(id).get();
    }

    public boolean existsDeviceById(int id){
        return repository.existsById(id);
    }

    public Device createDevice(Device Device) {
        return repository.save(Device);
    }

    public void updateDevice(int id, Device Device) {
        Device DeviceActual = repository.findById(id).get();
        DeviceActual.setId(Device.getId());
        DeviceActual.setVerifyCode(Device.getVerifyCode());
        DeviceActual.setBuild(Device.getBuild());
        repository.save(DeviceActual);
    }

    public void deleteDevice(int id) {
        repository.deleteById(id);
    }

}