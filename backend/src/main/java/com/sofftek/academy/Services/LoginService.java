package com.sofftek.academy.Services;

import com.sofftek.academy.Models.Login;
import com.sofftek.academy.Repositories.LoginRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * LoginService
 */
@Service
public class LoginService {

    @Autowired
    public LoginRepository repository;

 

    public LoginService() {

    }


    public Boolean existsByusernameAndPassword(String username, String password) {
        if(repository.existsByusername(username)){
            Login log =  repository.findByusername(username);
            return log.getPassword().equals(password);
        }else{
            return false;
        }
    }
}