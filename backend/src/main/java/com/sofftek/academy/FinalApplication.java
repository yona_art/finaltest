package com.sofftek.academy;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class FinalApplication extends SpringBootServletInitializer  {


	/**
	 * Esta aplicacion tiene implementados las vistas con un indice en /api/v1/ ademas
	 * puedes utilizar los diferentes rest controllers sumando al /api/v1/ 
	 * los cuales son controladores que regresan una respuesta json que hay los cuales son:
	 * builds, clocks, departaments, devices, peroid, perons, registers y users.
	 * Para utilizar el web service hay que utilizar solo /WS te mostrara las peticiones que existen
	 * utilizando SOAP requests.
	 */
	public static void main(String[] args) {
		SpringApplication.run(FinalApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	   return application.sources(FinalApplication.class);
 
	}

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

}
