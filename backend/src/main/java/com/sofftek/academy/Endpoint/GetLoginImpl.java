package com.sofftek.academy.Endpoint;

import javax.jws.WebService;

import org.apache.cxf.feature.Features;
import org.springframework.beans.factory.annotation.Autowired;

import com.jaom.ws.trainings.GetLoginRequest;
import com.jaom.ws.trainings.GetLoginResponse;
import com.jaom.ws.trainings.LoginPortType;
import com.sofftek.academy.Models.Login;
import com.sofftek.academy.Services.LoginService;

/**
 * GetLoginImpl
 */
@WebService
@Features(features = "org.apache.cxf.feature.LoggingFeature")
public class GetLoginImpl implements LoginPortType {

    // @Autowired
    // private LoginService loginService;

    private Login loginService;

    public GetLoginImpl() {
        init();
    }

    public void init() {
        loginService = new Login("admin", "admin");

    }

    @Override
    public GetLoginResponse getLogin(GetLoginRequest parameters) {
        GetLoginResponse response = new GetLoginResponse();
        response.setLogin((parameters.getUsername().equals(loginService.getUsername())
                && parameters.getPassword().equals(loginService.getPassword())));
        // Linea comentada ya que no funciona el autowired del servicio por ende se
        // instancia pero al instanciar el
        // repositorio fall por eso se simulo el flujo con el mismo record de base de
        // datos
        // response.setLogin(loginService.existsByusernameAndPassword(parameters.getUsername(),
        // parameters.getPassword()));
        return response;
    }

}