package com.sofftek.academy.Repositories;

import java.util.Date;
import java.util.List;

import com.sofftek.academy.Models.Person;
import com.sofftek.academy.Models.Register;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * RegisterRepository
 */
@Repository
public interface RegisterRepository extends JpaRepository<Register, Integer> {

    List<Register> findByperson(Person person);

    List<Register> findBydateBetweenAndPerson(Date start, Date end, Person person);

    List<Register> findBydateBetween(Date start, Date end);

}