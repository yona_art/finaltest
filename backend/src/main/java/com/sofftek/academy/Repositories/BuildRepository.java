package com.sofftek.academy.Repositories;

import com.sofftek.academy.Models.Build;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * BuildRepository
 */
@Repository
public interface BuildRepository extends JpaRepository<Build, Integer> {


    Build findByname(@Param("name") String name);

    boolean existsByName(String name);

}