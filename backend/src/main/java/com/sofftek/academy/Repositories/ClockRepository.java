package com.sofftek.academy.Repositories;

import com.sofftek.academy.Models.Clock;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * ClockRepository
 */
@Repository
public interface ClockRepository extends JpaRepository<Clock, Integer>{

    Clock findByName(@Param("name") String name);

    boolean existsByName(String name);
    
}