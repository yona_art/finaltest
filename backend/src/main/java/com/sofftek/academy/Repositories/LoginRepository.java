package com.sofftek.academy.Repositories;

import com.sofftek.academy.Models.Login;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * LoginRepository
 */
@Repository
public interface LoginRepository extends JpaRepository<Login, Integer>{

    boolean existsByusername(String username);
    
    Login findByusername(@Param("username") String name);
}