package com.sofftek.academy.Repositories;

import com.sofftek.academy.Models.Device;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * DeviceRepository
 */
@Repository
public interface DeviceRepository  extends JpaRepository<Device, Integer>{

    
}