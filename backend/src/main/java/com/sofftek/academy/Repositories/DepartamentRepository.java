package com.sofftek.academy.Repositories;

import com.sofftek.academy.Models.Departament;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * DepartamentRepository
 */
@Repository
public interface DepartamentRepository extends JpaRepository<Departament, Integer> {

    Departament findByName(@Param("name") String name);

    boolean existsByName(String name);
    
}