package com.sofftek.academy.Repositories;

import com.sofftek.academy.Models.Person;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * PersonRepository
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    Person findByis(@Param("is") String is);

    boolean existsByis(String is);

    Person findBynumber(@Param("number") int number);

    boolean existsBynumber(int number);
}