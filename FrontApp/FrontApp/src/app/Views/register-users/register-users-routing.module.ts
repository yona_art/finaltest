import { UsersListComponent } from './users-list/users-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'listGet'
  },
  {
    path: 'listGet',
    component: UsersListComponent,
    data: {
      isPost: false,
    }
  },
  {
    path: 'listPost',
    component: UsersListComponent,
    data: {
      isPost: true,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterUsersRoutingModule { }
