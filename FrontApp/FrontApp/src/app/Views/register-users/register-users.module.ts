import { AppMaterialModule } from './../../app-material.module';
import { UsersService } from './../../Core/Services/users.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterUsersRoutingModule } from './register-users-routing.module';
import { RegisterUsersComponent } from './register-users.component';
import { UsersListComponent } from './users-list/users-list.component';

@NgModule({
  declarations: [RegisterUsersComponent, UsersListComponent],
  imports: [
    CommonModule,
    RegisterUsersRoutingModule,
    AppMaterialModule,
  ],
  providers: [
    UsersService
  ]
})
export class RegisterUsersModule { }
