import { IListUsers } from './../../../Core/Interfaces/IListUsers';
import { tap, takeUntil, take } from 'rxjs/operators';
import { UsersService } from './../../../Core/Services/users.service';
import { Component, OnInit, ViewChild, AfterViewInit, Input, OnDestroy } from '@angular/core';
import { UsersDataSource } from 'src/app/Core/Datasource/users.datasource';
import { Subject, merge } from 'rxjs';
import { MatPaginator, MatSort } from '@angular/material';
import { ListRequestModel } from 'src/app/Core/Models/List.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() isPost: boolean = true;
  @Input() month: number;
  @Input() is: string;

  private ngUnSusbscribe = new Subject();
  DataSource: UsersDataSource;
  datasource_loaded = false;
  displayedColumns = ['id'];

  filterForm: FormGroup;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private usersService: UsersService, public inputFB: FormBuilder, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.createForm();
    this.DataSource = new UsersDataSource(this.usersService);
    this.isPost = this.activatedRoute.snapshot.params['isPost'];
  }

  ngAfterViewInit() {
    this.initTable();
  }

  initTable(): void {

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => {
          this.loadUsers();
        }),
        takeUntil(this.ngUnSusbscribe)
      ).subscribe();

    this.DataSource = new UsersDataSource(this.usersService);
    this.DataSource.connect().pipe(take(2)).subscribe(value => {
      this.datasource_loaded = !value;
    });
  }


  loadUsers(): void {
    const options = new ListRequestModel();
    options.filterPost = {
      is: this.filterForm.controls.is.value,
      month: this.filterForm.controls.month.value,
      startDate: this.filterForm.controls.start.value,
      endDate: this.filterForm.controls.end.value,
    } as IListUsers;
    options.pageSize = this.paginator.pageSize;
    options.pageIndex = this.paginator.pageIndex;
    if (!this.isPost) {
      this.DataSource.loadUsersPost(options);
    } else {
      this.DataSource.loadUsersGet(options);
    }
  }


  createForm(): void {
    this.filterForm = this.inputFB.group({
      is: [''],
      month: [{ value: '' }, []],
      start: [{ value: '' }, []],
      end: [{ value: '' }, []],
    });
  }

  ngOnDestroy(): void {
    this.ngUnSusbscribe.next();
    this.ngUnSusbscribe.complete();
  }

}
