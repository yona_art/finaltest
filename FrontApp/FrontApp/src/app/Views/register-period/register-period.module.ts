import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterPeriodRoutingModule } from './register-period-routing.module';
import { RegisterPeriodComponent } from './register-period.component';


@NgModule({
  declarations: [RegisterPeriodComponent],
  imports: [
    CommonModule,
    RegisterPeriodRoutingModule
  ]
})
export class RegisterPeriodModule { }
