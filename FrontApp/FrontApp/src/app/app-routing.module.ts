import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './Layout/default-layout/default-layout.component';
import { NotFoundComponent } from './Layout/not-found/not-found.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'users',
        loadChildren: () => import('./Views/register-users/register-users.module').then(m => m.RegisterUsersModule),
      },
      {
        path: 'period',
        loadChildren: () => import('./Views/register-period/register-period.module').then(m => m.RegisterPeriodModule),
      },
      {
        path: 'import',
        loadChildren: () => import('./Views/import/import.module').then(m => m.ImportModule),
      }
    ],
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
