export interface IListUsers {
    is: string;
    month: number;
    startDate: string;
    endDate: string;
}
