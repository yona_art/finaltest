import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';

import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    API = environment.API_ENDPOINT = 'registers';

    httpOptionsJSON = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    constructor(
        private httpClient: HttpClient
    ) { }

}
