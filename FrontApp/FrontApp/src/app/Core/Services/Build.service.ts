import { RegisterModel } from './../Models/Register.model';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class BuildService {

    API = environment.API_ENDPOINT = 'builds';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(
        private httpClient: HttpClient
    ) { }


    handleError(error: HttpErrorResponse) {
        return Observable.throw(error.error || 'Server Error');
    }
}

