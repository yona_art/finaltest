import { ListRequestModel, ListResponseModel } from '../Models/List.model';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PeriodService {

    API = environment.API_ENDPOINT = 'users';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(
        private httpClient: HttpClient
    ) { }

    public getListBymoth(listRequest: ListRequestModel): Observable<ListResponseModel> {
        return this.httpClient.get<ListResponseModel>(
            this.API + '/' + listRequest.filterPost.month,
            {
                params: new HttpParams()
                    .set('pageSize', listRequest.pageSize.toString())
                    .set('pageIndex', listRequest.pageIndex.toString()),
                headers: this.headers,
            }
        ).pipe(catchError(this.handleError));
    }

    public getListByDates(listRequest: ListRequestModel): Observable<ListResponseModel> {
        return this.httpClient.post<ListResponseModel>(this.API + '/', listRequest.filterPost,
            {
                params: new HttpParams()
                    .set('pageSize', listRequest.pageSize.toString())
                    .set('pageIndex', listRequest.pageIndex.toString()),
                headers: this.headers,
            }
        ).pipe(catchError(this.handleError));
    }


    handleError(error: HttpErrorResponse) {
        return Observable.throw(error.error || 'Server Error');
    }

}
