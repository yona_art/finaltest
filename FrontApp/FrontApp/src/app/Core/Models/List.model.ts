import { IListUsers } from '../Interfaces/IListUsers';
import { RegisterModel } from './Register.model';

export class ListRequestModel {
    public filterPost: IListUsers;
    public totalCount: number;
    public pageSize: number;
    public pageIndex: number;

    constructor() {
        this.defaultValues();
    }

    public defaultValues() {
        this.totalCount = 0;
        this.pageIndex = 0;
        this.pageSize = 0;
    }

}

export class ListResponseModel {
    public items: RegisterModel[];
    public hours: number;
    public totalCount: number;
}
