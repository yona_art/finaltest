export class ClockModel {
    public id: number;
    public name: string;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.name = '';
    }

    setValuesFromObject(object) {
        this.id = object.id;
        this.name = object.name;
    }

}
