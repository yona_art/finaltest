import { DepartamentModel } from './Departament.model';
import { PersonModel } from './Person.model';
import { DeviceModel } from './Device.model';
import { ClockModel } from './Clock.model';

export class RegisterModel {
    public id: number;
    public date: string;
    public departament: DepartamentModel | number;
    public peson: PersonModel | number;
    public device: DeviceModel | number;
    public clock: ClockModel | number;
    public UserExtFmt: number;

    setDefaultValues() {
        this.id = 0;
        this.date = '';
        this.departament = 0;
        this.peson = 0;
        this.device = 0;
        this.clock = 0;
        this.UserExtFmt = 0;
    }

    setValuesFromObject(object) {
        this.id = object.id;
        this.date = object.date;
        this.departament = object.departament;
        this.peson = object.person;
        this.device = object.device;
        this.clock = object.clock;
        this.UserExtFmt = object.UserExtFmt;
    }

}
