export class PersonModel {
    public id: number;
    public IS: string;
    public number: number;

    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.IS = '';
        this.number = 0;
    }

    setValuesFromObject(object) {
        this.id = object.id;
        this.IS = object.IS;
        this.number = object.number;
    }
}
