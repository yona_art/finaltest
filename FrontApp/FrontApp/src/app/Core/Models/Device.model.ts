export class DeviceModel {
    public id: number;
    public name: string;
    public build: DeviceModel | number;
    constructor() {
        this.setDefaultValues();
    }

    setDefaultValues() {
        this.id = 0;
        this.name = '';
        this.build = 0;
    }

    setValuesFromObject(object) {
        this.id = object.id;
        this.name = object.name;
        this.build = object.build;
    }
}
