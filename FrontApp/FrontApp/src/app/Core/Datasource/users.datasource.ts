import { UsersService } from './../Services/users.service';
import { BaseDataSource } from './base.datasource';
import { tap, catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs';
import { ListRequestModel } from '../Models/List.model';

export class UsersDataSource extends BaseDataSource {

    constructor(private userService: UsersService) {
        super();
    }

    loadUsersGet(ListReponse: ListRequestModel) {
        this.loadingSubject.next(true);
        this.userService.getListBydatesAndIS(ListReponse).pipe(
            tap(res => {
                this.entitySubject.next(res.items);
                this.paginatorTotalSubject.next(res.totalCount);
            }),
            catchError(err => of('error') ),
            finalize(() => this.loadingSubject.next(false)),
        ).subscribe();
    }

    loadUsersPost(ListReponse: ListRequestModel) {
        this.loadingSubject.next(true);
        this.userService.getListByDates(ListReponse).pipe(
            tap(res => {
                this.entitySubject.next(res.items);
                this.paginatorTotalSubject.next(res.totalCount);
            }),
            catchError(err => of('error') ),
            finalize(() => this.loadingSubject.next(false)),
        ).subscribe();
    }

}
