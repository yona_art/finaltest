import { UsersService } from './../Services/users.service';
import { BaseDataSource } from './base.datasource';
import { tap, catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs';
import { ListRequestModel } from '../Models/List.model';
import { PeriodService } from '../Services/Period.service';

export class PeriodDataSource extends BaseDataSource {

    constructor(private periodservice: PeriodService) {
        super();
    }

    loadUsersGet(ListReponse: ListRequestModel) {
        this.loadingSubject.next(true);
        this.periodservice.getListBymoth(ListReponse).pipe(
            tap(res => {
                this.entitySubject.next(res.items);
                this.paginatorTotalSubject.next(res.totalCount);
            }),
            catchError(err => of('error') ),
            finalize(() => this.loadingSubject.next(false)),
        ).subscribe();
    }

    loadUsersPost(ListReponse: ListRequestModel) {
        this.loadingSubject.next(true);
        this.periodservice.getListByDates(ListReponse).pipe(
            tap(res => {
                this.entitySubject.next(res.items);
                this.paginatorTotalSubject.next(res.totalCount);
            }),
            catchError(err => of('error') ),
            finalize(() => this.loadingSubject.next(false)),
        ).subscribe();
    }

}
